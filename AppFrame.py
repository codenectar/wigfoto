#!/usr/bin/env python3

# AppFrame.py – handles the overall display of the program

import wx

TEXT_CTRL_WIDTH = 300
FIXED = 0
STRETCH = 1

class AppFrame(wx.Frame):
    '''the frame that contains everything else'''

    def __init__(self, title, prev_cb, next_cb, quit_cb):
        '''given: frame title, callbacks for prev, next, and quit buttons
           returns: nothing '''
        super().__init__(parent=None, title=title)
        self.prev_cb = prev_cb
        self.next_cb = next_cb
        self.quit_cb = quit_cb

        # make a status bar, catch frame close events
        self.CreateStatusBar(2)
        self.Bind(wx.EVT_CLOSE, self.quit_cb)

        # ui panel and its items
        ui_panel = wx.Panel(self)
        self.ui_text  = wx.TextCtrl(ui_panel, size=(TEXT_CTRL_WIDTH, -1), style=wx.TE_MULTILINE)

        # button panel and its items
        but_panel = wx.Panel(ui_panel)
        self.but_prev = wx.Button(but_panel, -1, "<–––") 
        self.but_prev.Bind(wx.EVT_BUTTON, self.prev_cb)
        self.but_next = wx.Button(but_panel, -1, "––->")
        self.but_next.Bind(wx.EVT_BUTTON, self.next_cb)
        self.buts = {'prev': self.but_prev, 'next': self.but_next}

        # image panel and its items
        self.img_panel = wx.Panel(self)
        self.bitmap = wx.StaticBitmap(self.img_panel, bitmap=wx.Bitmap())

        # --- layout ---
        but_panel_sizer = wx.BoxSizer(wx.HORIZONTAL)
        but_panel_sizer.Add(self.but_prev, STRETCH, 0, 0)
        but_panel_sizer.Add(self.but_next, STRETCH, 0, 0)
        but_panel.SetSizer(but_panel_sizer)

        ui_panel_sizer = wx.BoxSizer(wx.VERTICAL)
        ui_panel_sizer.Add(but_panel, FIXED, wx.EXPAND, 0)
        ui_panel_sizer.Add(self.ui_text, STRETCH, 0, 0)
        ui_panel.SetSizer(ui_panel_sizer)

        img_panel_sizer = wx.BoxSizer(wx.VERTICAL)
        img_panel_sizer.Add(self.bitmap, FIXED, wx.LEFT, 0)
        self.img_panel.SetSizer(img_panel_sizer)

        frame_sizer = wx.BoxSizer(wx.HORIZONTAL)
        frame_sizer.Add(ui_panel, FIXED, wx.EXPAND, 0)
        frame_sizer.Add(self.img_panel, STRETCH, wx.EXPAND, 0)

        self.SetSizerAndFit(frame_sizer)
        self.Maximize()

    def set_button(self, name, state):
        '''set state of a button, return success'''
        if not name in self.buts: return False
        if state:
            self.buts[name].Enable()
        else:
            self.buts[name].Disable()
        return True

    def set_status(self, filename, uuid):
        '''set the values shown in the status bar'''
        self.SetStatusText(filename, 0)
        self.SetStatusText(uuid, 1)

    def get_text(self):
        '''return the contents of the text control'''
        return self.ui_text.GetValue()

    def set_text(self, value):
        '''set (actually change) the contents of the text control'''
        self.ui_text.ChangeValue(value) # does not generate event, as SetValue() does

    def set_image(self, path):
        '''replace image, scaling down if necessary'''
        # see how much space we have for image
        client_w, client_h = self.img_panel.GetClientSize()
        # load image and get its size
        image = wx.Image(path)
        image_w = image.GetWidth()
        image_h = image.GetHeight()
        # see how much scaling is needed in each direction
        scale_w = float(client_w) / float(image_w)
        scale_h = float(client_h) / float(image_h)
        # choose the more radical downsizing of the two
        scale = min(scale_w, scale_h)
        if scale < 1.0:
            # downsize image that is too big
            self.bitmap.SetBitmap(wx.Bitmap(image.Scale(scale * image_w, scale * image_h)))
        else:
            # image fits: just use it
            self.bitmap.SetBitmap(wx.Bitmap(image))

