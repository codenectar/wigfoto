#!/usr/bin/env python3

'''
wigfoto.py – a program to maintain a database of annotations to photos

Can read config from a Microsoft Windows INI file with a [DEFAULT] section:

    [DEFAULT]
    dbpath = ~/path/to/db/file

Can take alternate paths for config file or database file on command line.
'''

import os
import wx
import sys
import imghdr
import hashlib
import argparse
import configparser

import AppFrame
import Database

VERSION = 1.0
BLOCKLEN = 4096
CFGPATH = '~/.wigfoto/wigfoto.cfg'

class Manager:
    '''manage everything'''

    def __init__(self, title, db_file, images):
        '''store list of images names, make list for hashes, init database, create gui'''
        self.images = images
        self.hashes = [''] * len(images)
        self.index = 0
        self.db = Database.Database(db_file)
        self.app = wx.App()
        self.main_frame = AppFrame.AppFrame(title, self.prev_cb, self.next_cb, self.quit_cb)
        self.jump_to_image(0)

    def MainLoop(self):
        '''reveal window of app and enter the event loop'''
        self.main_frame.Show()
        self.app.MainLoop()

    def compute_hash(self, index):
        '''make a hash for the current image (if have not already)'''
        if not self.hashes[index] == '': return
        sha256_hash = hashlib.sha256()
        with open(self.images[index], "rb") as f:
            # read file and update hash string value in blocks
            for byte_block in iter(lambda: f.read(BLOCKLEN), b''):
                sha256_hash.update(byte_block)
            self.hashes[index] = sha256_hash.hexdigest()

    def update_display(self, notes):
        '''update state of display to match current situation'''
        # enable/disable nav buttons
        self.main_frame.set_button('prev', 0 < self.index)
        self.main_frame.set_button('next', self.index < len(self.images) - 1)

        self.main_frame.set_status(self.images[self.index], self.hashes[self.index])
        self.main_frame.set_image(self.images[self.index])
        self.main_frame.set_text(notes)

    def jump_to_image(self, new_index):
        '''save current state, move, and load new state'''
        if self.index != new_index:
            self.compute_hash(self.index)
            self.db.update(self.hashes[self.index], self.images[self.index], self.main_frame.get_text())
        self.index = new_index
        self.compute_hash(self.index)
        record = self.db.read(self.hashes[self.index])
        if record:
            self.update_display(record.notes)
        else:
            self.update_display('')

    def prev_cb(self, event):
        '''callback for press of PREV button'''
        if self.index <= 0: return
        self.jump_to_image(self.index - 1)

    def next_cb(self, event):
        '''callback for press of NEXT button'''
        if len(self.images) - 1 <= self.index: return
        self.jump_to_image(self.index + 1)

    def quit_cb(self, event):
        '''save final state as user quits the app'''
        self.compute_hash(self.index)
        self.db.update(self.hashes[self.index], self.images[self.index], self.main_frame.get_text())
        self.db.close()
        self.main_frame.Destroy()

def images_in_path(path):
    '''given: path to file or directory
       returns: list of paths to all images found'''
    filenames = []
    if os.path.isdir(path):
        (root, _, files) = next(os.walk(path))
        filenames = [os.path.join(root, file) for file in files]
    elif os.path.isfile(path):
        filenames = [path]
    images = [file for file in sorted(filenames) if imghdr.what(file)]
    return images

if __name__ == '__main__':

    # get the user's parameters for this run of the program
    dbpath = None
    cfgpath = os.path.expanduser(CFGPATH)
    parser = argparse.ArgumentParser(epilog=f'Default values are read from {cfgpath}')
    parser.add_argument('imgpath',         help='path to image or dir of images')
    parser.add_argument('--dbpath',  '-d', help='path to database file')
    parser.add_argument('--cfgpath', '-c', help='path to config file')
    parser.add_argument('--version', action='version', version=f'%(prog)s {VERSION}')
    args = parser.parse_args()
    if args.dbpath:
        dbpath = args.dbpath
    if args.cfgpath:
        cfgpath = args.cfgpath
        if not os.path.isfile(cfgpath):
            sys.exit(f'cannot find config file {cfgpath}')

    # read the config file, if any
    if os.path.isfile(cfgpath):
        config = configparser.RawConfigParser()
        config.read(cfgpath)
        if not dbpath:
            dbpath = os.path.expanduser(config['DEFAULT'].get('dbpath'))

    if not dbpath:
        sys.exit('no database path specified or found in config file')

    # make a list of the images selected
    images = images_in_path(args.imgpath)
    if not images:
        sys.exit(f'no image(s) found at {args.imgpath}')

    manager = None
    try:
        manager = Manager('wigfoto', dbpath, images)
        manager.MainLoop()
    except:
        raise

