## wigfoto

# Description
A python3 GUI application that helps with annotation and organization of folders of photos.

Can read config from a Microsoft Windows INI file with a [DEFAULT] section:

    [DEFAULT]
    dbpath = ~/path/to/db/file

Can take alternate paths for config file or database file on command line.

# Usage
Install required extra packages:
$ pip3 install -r requirements.txt

Running:
$ python3 wigfoto.py

Exit by just closing the app window: your final edits will be saved.

# Details
Files are identified by their SHA256 digest, so even if they move around the metadata you
enter will 'follow' them.  Unless the image is modified, then it is lost...

The data are stored in a SQLite database.  It is recommended that you back it up.
