#!/usr/bin/env python3

import os
import wx
import sys
import imghdr

TEXT_CTRL_WIDTH = 300
FIXED = 0
STRETCH = 1

class AppFrame(wx.Frame):

    def __init__(self, title, next_cb):
        super().__init__(parent=None, title=title)
        self.next_cb = next_cb

        self.CreateStatusBar(2)

        # ui panel and its items
        ui_panel = wx.Panel(self)
        ui_text  = wx.TextCtrl(ui_panel, size=(TEXT_CTRL_WIDTH, -1), style=wx.TE_MULTILINE)

        # button panel and its items
        but_panel = wx.Panel(ui_panel)
        but_prev = wx.Button(but_panel, -1, "<–––") 
        but_next = wx.Button(but_panel, -1, "––->")
        but_next.Bind(wx.EVT_BUTTON, self.next_cb)

        # image panel and its items
        self.img_panel = wx.Panel(self)
        self.img_panel.SetBackgroundColour(wx.Colour(255, 0, 0))
        self.bitmap = wx.StaticBitmap(self.img_panel, bitmap=wx.Bitmap())

        # --- layout ---
        but_panel_sizer = wx.StaticBoxSizer(wx.HORIZONTAL, but_panel, "but_panel")
        but_panel_sizer.Add(but_prev, STRETCH, 0, 0)
        but_panel_sizer.Add(but_next, STRETCH, 0, 0)
        but_panel.SetSizer(but_panel_sizer)

        ui_panel_sizer = wx.StaticBoxSizer(wx.VERTICAL, ui_panel, "ui_panel")
        ui_panel_sizer.Add(but_panel, FIXED, wx.EXPAND, 0)
        ui_panel_sizer.Add(ui_text, STRETCH, 0, 0)
        ui_panel.SetSizer(ui_panel_sizer)

        img_panel_sizer = wx.StaticBoxSizer(wx.VERTICAL, self.img_panel, "img_panel")
        img_panel_sizer.Add(self.bitmap, FIXED, wx.LEFT, 0)
        self.img_panel.SetSizer(img_panel_sizer)

        frame_sizer = wx.BoxSizer(wx.HORIZONTAL)
        frame_sizer.Add(ui_panel, FIXED, wx.EXPAND, 0)
        frame_sizer.Add(self.img_panel, STRETCH, wx.EXPAND, 0)

        self.SetSizerAndFit(frame_sizer)
        self.Maximize()

    def load_image(self, path):
        client_w, client_h = self.img_panel.GetClientSize()
        print(f'client size is {client_w} x {client_h}')

        image = wx.Image(path)
        image_w = image.GetWidth()
        image_h = image.GetHeight()

        print(f'image size is {image_w} x {image_h}')

        scale_w = float(client_w) / float(image_w)
        scale_h = float(client_h) / float(image_h)
        scale = min(scale_w, scale_h)

        print(f'scale factor is {scale}')

        if scale < 1.0:
            self.bitmap.SetBitmap(wx.Bitmap(image.Scale(scale * image_w, scale * image_h)))
        else:
            self.bitmap.SetBitmap(wx.Bitmap(image))

class Manager:

    def __init__(self, app, paths):
        self.app = app
        self.paths = paths
        self.index = 0
        self.frame = AppFrame('title', self.next_cb)

    def show(self):
        self.frame.Show()

    def next_cb(self, event):
        self.frame.load_image(self.paths[self.index])
        self.index += 1

def images_in_path(path):
    filenames = []
    if os.path.isdir(path):
        (root, _, files) = next(os.walk(path))
        filenames = [os.path.join(root, file) for file in files]
    elif os.path.isfile(path):
        filenames = [path]
    images = [file for file in sorted(filenames) if imghdr.what(file)]
    print(images)
    return images

if __name__ == '__main__':
    app = wx.App()

    width, height = wx.GetDisplaySize()
    print(f'display size is {width} x {height}')

    manager = Manager(app, images_in_path(sys.argv[1]))
    manager.show()
    app.MainLoop()

