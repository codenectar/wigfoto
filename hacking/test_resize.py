#!/usr/bin/env python3

# wigfoto.py

import wx

class ImageFrame(wx.Frame):
    '''frame that contains resizing image'''

    def __init__(self, image_path, title):
        super().__init__(parent=None, title=title)
        self.image = wx.Image(image_path)
        print(f'image size = {self.image.GetSize()}')
        self.static_bitmap = None
        self.Bind(wx.EVT_SIZE, self.onResize)

    def onResize(self, event):
        '''rescale the image to fit the new frame size'''
        frame_w, frame_h = self.GetSize()
        print(f'new frame size = {frame_w}, {frame_h}')
        image_w = self.image.GetWidth()
        image_h = self.image.GetHeight()
        scale_w = scale_h = 1.0
        if (frame_w < image_w):
            scale_w = float(frame_w) / float(image_w)
        if (frame_h < image_h):
            scale_h = float(frame_h) / float(image_h)
        scale = min(scale_w, scale_h)
        scaled_image = self.image.Scale(scale * image_w, scale * image_h)
        bitmap = wx.Bitmap(scaled_image)
        size = wx.Size(bitmap.GetWidth(), bitmap.GetHeight())
        print(f'bitmap size = {size}')
        if not self.static_bitmap:
            self.static_bitmap = wx.StaticBitmap(self, bitmap=bitmap)
        else:
            self.static_bitmap.SetBitmap(bitmap)

if __name__ == '__main__':
    app = wx.App()
    print(f'display size = {wx.GetDisplaySize()}')

    frame = ImageFrame('images/colombo.jpg', 'wigfoto')
    frame.Show()
    app.MainLoop()
