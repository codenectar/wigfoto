#!/usr/bin/env python3

# Database.py – handles persisting the user data

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Image(Base):
    __tablename__ = 'image'

    id       = Column(Integer, primary_key=True)
    digest   = Column(String)
    filename = Column(String)
    notes    = Column(String)

    def __repr__(self):
        return f'<Image>\n' \
               f'id       | {self.id}\n' \
               f'digest   | {self.digest}\n' \
               f'filename | {self.filename}\n' \
               f'notes    | {self.notes}'

class Database:
    '''manage the persistence of user data'''

    def __init__(self, db_file):
        self.engine = create_engine(f'sqlite:///{db_file}', echo=True)
        Base.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def create(self, digest, filename, notes, skip_check=False):
        '''create record if digest not found'''
        # find any existing instance by digest (better not find any)
        if skip_check or self.session.query(Image).filter_by(digest=digest).first() == None:
            self.session.add(Image(digest=digest, filename=filename, notes=notes))
            self.session.commit()

    def read(self, digest):
        '''return the record (if any) with given digest'''
        rs = self.session.query(Image).filter_by(digest=digest).all()
        assert len(rs) < 2  # eek – DB contains duplicates with same digest
        if rs: return rs[0]
        return None

    def update(self, digest, filename, notes):
        '''update record if digest found, else create'''
        image = self.read(digest)
        if image:
            image.filename = filename
            image.notes = notes
            self.session.commit()
        else:
            self.create(digest, filename, notes, skip_check=True)

    def delete(self):
        pass

    def close(self):
        self.session.close()

if __name__ == '__main__':
    # for testing
    db = Database('_foo.db')
    db.update('11111', 'foo.jpg', 'first record')
    db.create('11111', 'bar.png', 'should be ignored')
    db.update('11111', 'BAR.jpg', 'updated first record')
    db.create('22222', 'baz.jpg', 'second record')
    print(db.read('11111'))
    print(db.read('22222'))
    db.close()

